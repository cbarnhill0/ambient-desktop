import time

# Returns the time of day as a string ("morning", "day", etc)
def get_time_of_day():
    current_hour = time.localtime().tm_hour
    
    if(current_hour >= 5 and current_hour <= 11):
        return "morning"
    elif(current_hour >= 12 and current_hour <= 17):
        return "day"
    elif(current_hour >= 18 and current_hour <= 20):
        return "afternoon"
    elif(current_hour >= 21 or current_hour <= 4):
        return "night"