# Ambient Desktop
A python script for automatically changing a computer's desktop wallpaper based on the time of day.

## Usage
- Optional: Add your own photos to each of the time of day subfolders in the main "images" folder.

- Run the script like any other Python program:

    - `$ python3 main.py`

## System Requirements
- Linux OS (Only tested on Ubuntu 16.04)

- Microsoft Windows (Only tested on Windows 7 64-bit)