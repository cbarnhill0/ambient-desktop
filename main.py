import os
import time
import sys
import ambient_time as ambt
import wallpaper

def main():
    # Directory of this file
    current_dir = os.path.dirname(os.path.realpath(__file__))
    # Main loop
    while(True):
        # Create an array of images for the current time of day
        file_names = []
        current_time_of_day = ambt.get_time_of_day()
        for (path, dir_name, file_name) in os.walk(current_dir + "/images/" + current_time_of_day):
            file_names = file_name

        # Change the background for the current time of day every 30 seconds
        for file_name in file_names:
            wallpaper.change(current_time_of_day, file_name)
            time.sleep(30)
            continue

if __name__ == "__main__":
    main()