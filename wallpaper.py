import os
import ctypes

# Change the desktop background
def change(folder_path, image_name):
    current_dir = os.path.dirname(os.path.realpath(__file__))

    # Check the OS and change the background image
    # This is only works for GNOME (gsettings) and Windows. More may be added later
    os_name = os.name
    if(os_name == "posix"):
        image_path = "file://" + current_dir + "/images/" + folder_path + "/" + image_name
        if(os.path.exists("/usr/bin/gsettings")):
            # Change wallpaper image in gsettings
            os.system("/usr/bin/gsettings set org.gnome.desktop.background picture-uri '" + image_path + "'")
    elif(os_name == "nt"):
        image_path = current_dir + "\\images\\" + folder_path + "\\" + image_name
        # Windows specific wallpaper change function
        SPI_SETDESKWALLPAPER = 20 
        ctypes.windll.user32.SystemParametersInfoW(SPI_SETDESKWALLPAPER, 0, image_path, 0)

